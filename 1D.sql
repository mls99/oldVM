-- make view with only sen
CREATE VIEW sen_nc AS
SELECT p.id, p.first_name, p.last_name, r.type, p.birthday
FROM persons p, person_roles r
WHERE p.id = r.person_id AND r.state = 'NC' AND r.type = 'sen'
GROUP BY p.id, r.type;
-- make view with only rep
CREATE VIEW rep_nc AS
SELECT p.id, p.first_name, p.last_name, r.type, p.birthday
FROM persons p, person_roles r
WHERE p.id = r.person_id AND r.state = 'NC' AND r.type = 'rep'
GROUP BY p.id, r.type;
-- join only those p.id that exist in both view tables
SELECT r.id, r.first_name, r.last_name, r.type, s.type, r.birthday
FROM sen_nc s, rep_nc r
WHERE s.id = r.id
ORDER BY r.id;