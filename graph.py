#!/usr/bin/python

import argparse
import numpy
import heapq
import graph_tool
from graph_tool import Graph, generation, stats, clustering
import sys
import os
import matplotlib
if not os.environ.get('DISPLAY'):
    matplotlib.use('PDF')
from matplotlib import pyplot
import itertools

######################################################################
def graph_random(N, p):
    # generate a random graph to play with:
    G = generation.random_graph(\
            N,\
            lambda: numpy.random.binomial(N, p),\
            directed=False)
    # you can annotate each vertex with additional properties:
    G.vertex_properties['name'] = G.new_vertex_property('string')
    for v in G.vertices():
        G.vertex_properties['name'][v] = hex(int(v))
    return G

######################################################################
def graph_from_file(filename):
    nodemap = dict()
    G = Graph(directed=False)
    G.vertex_properties['name'] = G.new_vertex_property('string')

    def get_node(character):
        if character in nodemap:
            return nodemap[character]
        else:
            v = G.add_vertex()
            nodemap[character] = v
            G.vertex_properties['name'][v] = character
            return v

    with open(filename) as gfile:
        print 'edges processed:',
        sys.stdout.flush()
        for lno, line in enumerate(gfile, start=1):
            char1, char2 = line.strip().split('\t')
            char1 = char1.strip('"')
            char2 = char2.strip('"')
            G.add_edge(get_node(char1), get_node(char2))
            if lno % 10000 == 0:
                print lno,
                sys.stdout.flush()
        print 'done'

    return G

######################################################################
parser = argparse.ArgumentParser()
parser.add_argument('gfilename', nargs='?',
                    help='file with the graph, .tsv or .xml.gz')
parser.add_argument('--batch', action='store_true',
                    help='non-interactive mode; figure output in .pdf')
args = parser.parse_args(sys.argv[1:])
if args.gfilename:
    if args.gfilename.endswith('.tsv'):
        G = graph_from_file(args.gfilename)
    else:
        G = graph_tool.load_graph(args.gfilename)
    save_prefix = os.path.splitext(os.path.split(args.gfilename)[-1])[0]
else:
    G = graph_random(1000, 0.01)
    save_prefix = 'random'

######################################################################
print '{} nodes, {} edges'.format(G.num_vertices(), G.num_edges())

######################################################################
# degree distribution:
print '='*70

degrees = [v.out_degree() for v in G.vertices()]

topn = 20
print '* top {} nodes in terms of degree:'.format(topn)
for i, deg in heapq.nlargest(topn, enumerate(degrees), lambda x: x[1]):
    print '  degree {}: {}'.format(deg, i),
    for prop in G.vertex_properties.values():
        print prop[G.vertex(i)],
    print

deg_pdf, deg_bins = numpy.histogram(\
    degrees,\
    bins=numpy.amax(degrees)-numpy.amin(degrees)+1,\
    density=True)

fig = matplotlib.pyplot.figure()
ax1 = fig.add_subplot(1, 2, 1)
ax1.plot(deg_bins[:-1], deg_pdf, 'x')
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlabel('degree k')
ax1.set_ylabel('fraction of nodes with degree k')

ax2 = fig.add_subplot(1, 2, 2)
deg_ccdf = 1.0 - numpy.cumsum(deg_pdf)
rects2 = ax2.plot(deg_bins[:-1], deg_ccdf, 'x')
ax2.set_xscale('log')
ax2.set_yscale('log')
ax2.set_xlabel('degree k')
ax2.set_ylabel('fraction of nodes with degree >= k')

if os.environ.get('DISPLAY') and not args.batch:
    matplotlib.pyplot.show()
fig.savefig(save_prefix + '-deg.pdf', format='PDF')

######################################################################
# distance distribution:
print '='*70

dist_counts, dist_bins = stats.distance_histogram(G)

diameter = dist_bins[-2]
print '* diameter = {}'.format(diameter)

if diameter <= 20:
    print '* distance distribution (# hops = d vs. # node pairs with distance = d):'
    for dist, count in zip(dist_bins, dist_counts):
        print '  {}: {}'.format(dist, count)

fig = matplotlib.pyplot.figure()
ax = fig.add_subplot(1, 1, 1)
dist_cdf = numpy.cumsum(dist_counts / sum(dist_counts))
ax.plot(dist_bins[:-1], dist_cdf, '-x')
ax.set_xlabel('d hops')
ax.set_ylabel('fraction of node pairs with distance <= d hops')
if os.environ.get('DISPLAY') and not args.batch:
    matplotlib.pyplot.show()
fig.savefig(save_prefix + '-dist.pdf', format='PDF')

######################################################################
# clustering coefficients:
print '='*70

cluster_coeffs = clustering.local_clustering(G).get_array()

print '* clustering coefficients'
print '  min  : {}'.format(numpy.amin(cluster_coeffs))
print '  mean : {}'.format(numpy.mean(cluster_coeffs))
print '  stdev: {}'.format(numpy.std(cluster_coeffs))
print '  max  : {}'.format(numpy.amax(cluster_coeffs))

######################################################################
# my clustering coefficients:

def my_compute_clustering_coeffients(G):
    # WARNING: THIS IS TEMPLATE CODE TO GET YOU STARTED.  IT DOESN'T
    # COMPUTE THE CORRECT CLUSTERING COEFFICIENTS.  YOU NEED TO
    # REPLACE IT WITH THE CORRECT IMPLEMENTATION.
    cluster_coeffs = list()
    print 'nodes processed:',
    sys.stdout.flush()
    for v in G.vertices():
        degree = v.out_degree()
        if degree <= 1:
            cluster_coeffs.append(0.0)
        else:
            count = 0
	    for n1 in v.out_neighbors():
		for n2 in v.out_neighbors():
		    if n2 in n1.out_neighbors() and n2 != n1:
		        count += 0.5
	    total = (2*count)/(degree*(degree-1))
	    cluster_coeffs.append(total)
        if (int(v)+1) % 100 == 0:
            print int(v)+1,
            sys.stdout.flush()
    print 'done'
    return cluster_coeffs

# UNCOMMENT THE STATEMENTS BELOW TO TEST YOUR CODE:

print '='*70

cluster_coeffs = my_compute_clustering_coeffients(G)

print '* clustering coefficients I computed'
print '  min  : {}'.format(numpy.amin(cluster_coeffs))
print '  mean : {}'.format(numpy.mean(cluster_coeffs))
print '  stdev: {}'.format(numpy.std(cluster_coeffs))
print '  max  : {}'.format(numpy.amax(cluster_coeffs))
