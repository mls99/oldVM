SELECT p.first_name, p.last_name, p.birthday, r.start_date, r.end_date
FROM persons p, person_roles r
WHERE p.id = r.person_id AND r.party = 'Democrat' AND r.state = 'NC'
ORDER BY r.start_date DESC