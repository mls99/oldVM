-- view orders by age and limits to first 25
CREATE VIEW young_members AS
SELECT p.id, p.first_name, p.last_name, p.birthday, p.state, 
(date_part('year', current_date) - date_part('year', p.birthday)) AS age
FROM cur_members p
ORDER BY age
LIMIT 25;
-- counts and orders by state
SELECT state, COUNT(*)
FROM young_members
GROUP BY state;