-- for some reason cur_members was giving me errors so I copied the code here
CREATE VIEW newcurr AS
SELECT p.id, p.first_name, p.last_name, r.party, r.state
FROM persons p, person_roles r
WHERE p.id = r.person_id AND r.start_date <= current_date AND current_date <= r.end_date;

-- joined cur_members and person_votes, removing non-person votes
CREATE VIEW newvoted AS
SELECT n.id, n.first_name, n.last_name, n.state, v.vote
FROM person_votes v, newcurr n
WHERE n.id = v.person_id AND v.vote NOT IN ('Yea','Aye','Nay','No','Present','Not Voting');

-- group by votes
SELECT vote, COUNT(*)
FROM newvoted
GROUP BY vote
ORDER BY COUNT(*) DESC;

