#!/usr/bin/env python

import sys
import collections

# WARNING: THIS IS TEMPLATE CODE TO GET YOU STARTED.  IT SIMPLY READS IN
# character_issues.tsv AND WRITES THE CONTENT OUT AS characters.tsv.
# YOU NEED TO REPLACE IT WITH THE CORRECT IMPLEMENTATION.

######################################################################

chars = set()
issues = set()
edge_set = set()
edges = list()
social_dict = collections.defaultdict(list)

with open('character_issues.tsv') as gfile:
	print 'edges processed:',
	for lno, line in enumerate(gfile, start=1):
		char, issue = line.strip().split('\t')
		char = char.strip('"')
		issue = issue.strip('"')
		chars.add(char)
		issues.add(issue)
		social_dict[issue].append(char)
		if len(social_dict[issue]) > 1:
			for pal in social_dict[issue]:
				if pal != char:
					if pal < char:
						edge_set.add((pal, char))
					else:
						edge_set.add((char, pal))
		# edges.append((char, issue))
		if lno % 10000 == 0:
			print lno,
			sys.stdout.flush()
	print 'done'
print '{} characters, {} issues'.format(len(chars), len(issues))

edges = sorted(edge_set)
# for u, v in edges:
# 	social_dict[v].append(u)

######################################################################

with open('characters.tsv', 'w') as gfile:
	for u, v in edges:
		gfile.write('"{}"\t"{}"\n'.format(u, v))
