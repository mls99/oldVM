CREATE VIEW TP(id1, id2) AS
-- Compute the match pairs that are *true positives* with respect to
-- the official answers in official_matches:
SELECT id1, id2 FROM matches
INTERSECT
SELECT id1, id2 FROM official_matches;

CREATE VIEW FP(id1, id2) AS
-- Compute the match pairs that are *false positives* with respect to
-- the official answers in official_matches:
SELECT id1, id2 FROM matches
EXCEPT
SELECT id1, id2 FROM official_matches;

CREATE VIEW FN(id1, id2) AS
-- Compute the match pairs that are *false negatives* with respect to
-- the official answers in official_matches:
SELECT id1, id2 FROM official_matches
EXCEPT
SELECT id1, id2 FROM matches;
