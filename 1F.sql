CREATE VIEW table3 AS
SELECT p.id, p.first_name, p.last_name, r.party, r.state
FROM persons p, person_roles r
WHERE p.id = r.person_id AND r.start_date <= current_date AND current_date <= r.end_date;

CREATE VIEW table4 AS
SELECT a.id, a.state, v.vote
FROM table3 a, person_votes v
WHERE a.id = v.person_id AND v.vote = 'Ryan (WI)';

SELECT state, COUNT(*)
FROM table4
GROUP BY state
ORDER BY COUNT(*) DESC;