-- match query:
SELECT A.id, B.id
FROM restaurants A, restaurants B
WHERE similarity(trim(lower(A.name)), trim(lower(B.name))) >= 0.5
--WHERE similarity(regexp_replace(trim(lower(A.name)), '\(.*\)', ''), regexp_replace(trim(lower(B.name)), '\(.*\)', '')) >= 0.5
AND (similarity(trim(lower(A.city)), trim(lower(B.city))) >= 0.5
OR similarity(trim(lower(A.addr)), trim(lower(B.addr))) >= 0.5)
--AND similarity(regexp_replace(trim(lower(A.type)), '\(.*\)', ''), regexp_replace(trim(lower(B.type)), '\(.*\)', '')) >= 0.5
AND A.id < B.id;


--WHERE similarity(regexp_replace(trim(lower(A.name)), '\(.*\)', ''), regexp_replace(trim(lower(B.name)), '\(.*\)', '')) >= 0.5